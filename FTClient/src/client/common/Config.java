package client.common;

public final class Config {

	public static final String HOST = "10.151.34.95";
	public static final int PORT_CTRL = 6666;
	public static final int PORT_DATA = 6667;
	public static final int PORT_NOTIF = 6668;
	public static final byte BUFFER_SIZE_BYTE = (byte) 8192;
	public static final int BUFFER_SIZE_INT = 8192;
	
}
