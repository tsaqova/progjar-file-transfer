package client.common;

import client.gui.Window;

public final class Command {

	public static final String STOR = "STOR";
	public static final String LIST = "LIST";
	public static final String OK = "OK";
	public static final String OK_CTRL = OK + " " + Config.PORT_CTRL;
	public static final String OK_DATA = OK + " " + Config.PORT_DATA;
	public static final String OK_NOTIF = OK + " " + Config.PORT_NOTIF;
	
	public static final String SendCommand(String listUser) {
		return "STOR " + listUser + " " + Window.getFile().getName();
	}
	
}
