package client;

import java.io.IOException;
import java.net.UnknownHostException;
import client.gui.Window;
import client.thread.ControlThread;
import client.thread.NotifThread;
import client.user.UserList;

public final class FTClient {
	
	private static ControlThread controlThread;
	private static NotifThread notifThread;
	private static String host;
	private static int port;
	private static UserList userList;
	
	public FTClient() throws UnknownHostException, IOException {
		Window window = Window.getInstance();
		window.setVisible(true);
		setUserList(new UserList());
		setControlThread(new ControlThread(this));
		setNotifThread(new NotifThread(this));
	}
	
	public static void start() throws IOException{
		controlThread.start();
		notifThread.start();
	}

	public static ControlThread getControlThread() {
		return controlThread;
	}

	public static void setControlThread(ControlThread controlThread) {
		FTClient.controlThread = controlThread;
	}

	public static NotifThread getNotifThread() {
		return notifThread;
	}

	public static void setNotifThread(NotifThread notifThread) {
		FTClient.notifThread = notifThread;
	}
	
	public static String getHost() {
		return host;
	}

	public static void setHost(String host) {
		FTClient.host = host;
	}

	public static int getPort() {
		return port;
	}

	public static void setPort(int port) {
		FTClient.port = port;
	}
	
	public static UserList getUserList() {
		return userList;
	}

	public static void setUserList(UserList userList) {
		FTClient.userList = userList;
	}
	
}
