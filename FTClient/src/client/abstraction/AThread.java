package client.abstraction;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import client.FTClient;

public abstract class AThread implements Runnable {

	private Thread thread;
	private Socket socket;
	private BufferedReader br;
	private PrintWriter pw;
	private FTClient ftClient;
	
	public AThread() {
		
	}

    public void start() {
    	thread = new Thread(this);
    	thread.start();
    }
    
	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	public PrintWriter getPw() {
		return pw;
	}
	
	public void setPw(PrintWriter printWriter) {
		this.pw = printWriter;
	}
	

	public BufferedReader getBr() {
		return br;
	}

	public void setBr(BufferedReader br) {
		this.br = br;
	}

	public FTClient getFtClient() {
		return ftClient;
	}

	public void setFtClient(FTClient ftClient) {
		this.ftClient = ftClient;
	}
	
	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}
	
}
