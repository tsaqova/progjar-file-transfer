package client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;

import client.FTClient;

public class RefreshButton extends JButton {

	public RefreshButton() {
		super("Refresh");
		
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(FTClient.getControlThread().getThread() != null) {
						FTClient.getControlThread().refreshUser();
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		});
	}
	
}
