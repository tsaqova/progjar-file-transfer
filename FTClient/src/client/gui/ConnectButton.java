package client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;

import client.FTClient;

public class ConnectButton extends JButton {
	
	public ConnectButton() {
		super("Connect");
		
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(getText().equals("Connect")) {
					FTClient.setHost(Window.getInstance().getHostField().getText());
					FTClient.setPort(Integer.parseInt(Window.getInstance().getPortField().getText()));
					try {
						FTClient.start();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				else if(getText().equals("Disconnect")) {
					FTClient.getControlThread().getThread().interrupt();
					setText("Connect");
					FTClient.getUserList().removeAll();
					Window.getInstance().updateTable();
				}
			}
		});
		
	}

	
}
