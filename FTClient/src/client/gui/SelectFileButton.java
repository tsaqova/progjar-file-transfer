package client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;

public class SelectFileButton extends JButton {

	public SelectFileButton() {
		super("File...");
		
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();
				int retval = fc.showOpenDialog(Window.getInstance());
				if(retval == JFileChooser.APPROVE_OPTION) {
					Window.setFile(fc.getSelectedFile());
					System.out.println("File " + fc.getName() + " selected");
				}
			}
		});
	}
	
}
