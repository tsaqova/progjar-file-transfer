package client.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import client.FTClient;
import client.user.User;

public final class Window extends JFrame {

	private static Window instance = new Window();
	private static File file;
	private JPanel contentPanel;
	private JTextField hostField;
	private JTextField portField;
	private UserTable table;
	private ConnectButton connectBtn;
	private JScrollPane scrollPane;
	private RefreshButton btnRefresh;
	private SelectFileButton btnSelectFile;
	private SendButton btnSend;
	
	private Window() {
		setTitle("File Transfer Client");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 383, 313);
		initWindowComponent();
		addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            	if(FTClient.getControlThread().getThread() != null){
            		FTClient.getControlThread().getThread().interrupt();
            	}
                System.exit(0);
            }
        });
	}

	private void initWindowComponent() {
		setContentPanel(new JPanel());
		setContentPane(contentPanel);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setLayout(null);
		
		setConnectBtn(new ConnectButton());
		getConnectBtn().setBounds(270, 11, 89, 23);
		contentPanel.add(connectBtn);
		
		setHostField(new JTextField("10.151.34.95"));
		getHostField().setBounds(38, 12, 108, 20);
		getHostField().setColumns(10);
		contentPanel.add(hostField);
		
		JLabel lblHost = new JLabel("Host");
		lblHost.setBounds(10, 15, 46, 14);
		contentPanel.add(lblHost);
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(156, 15, 46, 14);
		contentPanel.add(lblPort);
		
		setPortField(new JTextField("6666"));
		getPortField().setBounds(189, 12, 71, 20);
		getPortField().setColumns(10);
		contentPanel.add(portField);
		
		JLabel lblUserList = new JLabel("User List :");
		lblUserList.setBounds(10, 44, 60, 14);
		contentPanel.add(lblUserList);
		
		setTable(new UserTable());
        table.getTable().setPreferredScrollableViewportSize(table.getTable().getPreferredSize());
        setScrollPane(new JScrollPane(table.getTable()));
        scrollPane.setBounds(10, 69, 250, 169);
        contentPanel.add(scrollPane);
        
        setBtnRefresh(new RefreshButton());
        getBtnRefresh().setBounds(270, 65, 89, 23);
    	contentPanel.add(btnRefresh);
    	
    	setBtnSelectFile(new SelectFileButton());
		getBtnSelectFile().setBounds(68, 244, 93, 23);
		contentPanel.add(btnSelectFile);
		
		setBtnSend(new SendButton());
		getBtnSend().setBounds(171, 244, 89, 23);
		contentPanel.add(btnSend);
	}
	
	public void updateTable() {
		Vector<Object> columnNames = new Vector<Object>();
		columnNames.add("address");
		columnNames.add("send");
		columnNames.add("block");
		Vector<Object> data = new Vector<Object>();
		HashMap<String, User> userMap = FTClient.getUserList().getUserMap();
		for(String user : userMap.keySet()) {
			Vector<Object> userData = new Vector<Object>();
			userData.add(user);
			userData.add(userMap.get(user).isSend());
			userData.add(userMap.get(user).isBlock());
			data.add(userData);
		}
        table.setModel(new DefaultTableModel(data, columnNames));
        table.setTable(new JTable(table.getModel()) {
            private static final long serialVersionUID = 1L;
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return String.class;
                    default:
                        return Boolean.class;
                }
            }
        });
        table.getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				int row = e.getFirstRow();
		        int column = e.getColumn();
		        TableModel model = (TableModel)e.getSource();
		        String columnName = model.getColumnName(column);
		        Object data = model.getValueAt(row, column);
		        Object address = model.getValueAt(row, 0);
		        if(columnName.equals("send")) {
		        	FTClient.getUserList().getUserMap().get(address.toString()).setSend(Boolean.parseBoolean(data.toString()));
		        }
		        else if(columnName.equals("block")) {
		        	FTClient.getUserList().getUserMap().get(address.toString()).setBlock(Boolean.parseBoolean(data.toString()));
		        }
			}
		});
        table.getTable().setPreferredScrollableViewportSize(table.getTable().getPreferredSize());
        contentPanel.remove(scrollPane);
        scrollPane = new JScrollPane(table.getTable());
        scrollPane.setBounds(10, 69, 250, 169);
        contentPanel.add(scrollPane);
	}
	
	public static Window getInstance() {
		return instance;
	}

	public static File getFile() {
		return file;
	}

	public static void setFile(File file) {
		Window.file = file;
	}

	public JPanel getContentPanel() {
		return contentPanel;
	}

	public void setContentPanel(JPanel contentPanel) {
		this.contentPanel = contentPanel;
	}

	public JTextField getHostField() {
		return hostField;
	}

	public void setHostField(JTextField hostField) {
		this.hostField = hostField;
	}

	public JTextField getPortField() {
		return portField;
	}

	public void setPortField(JTextField portField) {
		this.portField = portField;
	}

	public UserTable getTable() {
		return table;
	}

	public void setTable(UserTable table) {
		this.table = table;
	}

	public ConnectButton getConnectBtn() {
		return connectBtn;
	}

	public void setConnectBtn(ConnectButton connectBtn) {
		this.connectBtn = connectBtn;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public RefreshButton getBtnRefresh() {
		return btnRefresh;
	}

	public void setBtnRefresh(RefreshButton btnRefresh) {
		this.btnRefresh = btnRefresh;
	}

	public SelectFileButton getBtnSelectFile() {
		return btnSelectFile;
	}

	public void setBtnSelectFile(SelectFileButton btnSelectFile) {
		this.btnSelectFile = btnSelectFile;
	}

	public SendButton getBtnSend() {
		return btnSend;
	}

	public void setBtnSend(SendButton btnSend) {
		this.btnSend = btnSend;
	}
	
}
