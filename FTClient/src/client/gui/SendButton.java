package client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;

import client.FTClient;

public class SendButton extends JButton {

	public SendButton() {
		super("Send");
		
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					FTClient.getControlThread().sendFile();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
}
