package client.gui;

import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class UserTable {

	private DefaultTableModel model;
	private JTable table;
	
	public UserTable() {
		Vector<Object> columnNames = new Vector<Object>();
		columnNames.add("address");
		columnNames.add("send");
		columnNames.add("block");
		Vector<Object> data = new Vector<Object>();
	    model = new DefaultTableModel(data, columnNames);
	    table = new JTable(model) {
	        private static final long serialVersionUID = 1L;
			@Override
	        public Class getColumnClass(int column) {
	            switch (column) {
	                case 0:
	                    return String.class;
	                default:
	                    return Boolean.class;
	            }
	        }
	    };
	}
	
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}
	
	public DefaultTableModel getModel() {
		return model;
	}

	public void setModel(DefaultTableModel model) {
		this.model = model;
	}
	
}
