package client.thread;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import client.FTClient;
import client.abstraction.AThread;
import client.common.Config;

public class NotifThread extends AThread {
	
	public NotifThread(FTClient ftClient) throws UnknownHostException, IOException {
		setFtClient(ftClient);
	}
	
	public void run() {
		String msg;
		try {
			setSocket(new Socket(FTClient.getHost(), Config.PORT_NOTIF));
			setBr(new BufferedReader(new InputStreamReader(getSocket().getInputStream())));
			setPw(new PrintWriter(getSocket().getOutputStream()));
			while((msg = getBr().readLine()) != null) {
				Scanner sc = new Scanner(msg);
				String arg[] = {"", "", ""};
				for(int i = 0; i<3; i++) {
					if(i<2) {
						if(sc.hasNext())arg[i] = sc.next();
					}
					else if(sc.hasNextLine())arg[i] = sc.nextLine();
				}
				if(!FTClient.getUserList().getUserMap().get(arg[0]).isBlock()) {
					getPw().println("OK");
					getPw().flush();
					Socket newSocket = new Socket(FTClient.getHost(), Integer.parseInt(arg[1]));
					byte[] bytearray = new byte[8192];
					InputStream is = newSocket.getInputStream();
					FileOutputStream fos = new FileOutputStream(arg[2]);
					BufferedOutputStream bos = new BufferedOutputStream(fos);
					int bytesRead;
					while((bytesRead = is.read(bytearray)) > 0) {
						bos.write(bytearray, 0, bytesRead);
					}
					bos.flush();
					bos.close();
					newSocket.close();
				}
				else {
					getPw().println("NO");
					getPw().flush();
				}
				sc.close();
			}
		}
		catch( NoSuchElementException ex) {
			ex.printStackTrace();
		}
		catch (IOException e) {
			
		}
	}

}
