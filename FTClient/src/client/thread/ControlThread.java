package client.thread;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import sun.security.krb5.Config;
import client.FTClient;
import client.abstraction.AThread;
import client.common.Command;
import client.common.Message;
import client.gui.Window;
import client.user.User;

public class ControlThread extends AThread {
	
	public ControlThread(FTClient ftClient) throws UnknownHostException, IOException {
		setFtClient(ftClient);
	}
	
	public void run() {
		try {
			setSocket(new Socket(FTClient.getHost(), FTClient.getPort()));
			setBr(new BufferedReader(new InputStreamReader(getSocket().getInputStream())));
			setPw(new PrintWriter((getSocket().getOutputStream())));
			Window.getInstance().getConnectBtn().setText("Disconnect");
			while(!getThread().isInterrupted()) {
				// Waiting event on Window GUI
			}
			getSocket().close();
			getPw().close();
			getBr().close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(Window.getInstance().getContentPane(), Message.CANNOTCONNECT);
		}
	}

	public void sendFile() throws IOException {
		try{			
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(Window.getFile()));
			String listUser = ";";
			for(String user : FTClient.getUserList().getUserMap().keySet()) {
				if(FTClient.getUserList().getUserMap().get(user).isSend()) {
					listUser += user + ";";
				}
			}
			getPw().println(Command.SendCommand(listUser));
			getPw().flush();
			String response = getBr().readLine();
			Scanner scr = new Scanner(response);
			String[] arg = {"", "", ""};
			for(int i=0; i<3 && scr.hasNext(); i++) {
				arg[i] = scr.next();
			}
			if(arg[0].equals(Command.OK)){
				Socket newSocket = new Socket(FTClient.getHost(), Integer.parseInt(arg[1]));
				byte[] mybytearray = new byte[(int) Window.getFile().length()];
				bis.read(mybytearray, 0, mybytearray.length);
				OutputStream os = newSocket.getOutputStream();
				os.write(mybytearray, 0, mybytearray.length);
				os.flush();
				os.close();
				bis.close();
				newSocket.close();
				JOptionPane.showMessageDialog(Window.getInstance().getContentPane(), Message.TRANSFERED);
			}
			scr.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("File Not Found");
		}
	}
	
	public void refreshUser() throws IOException {
		getPw().println(Command.LIST);
		getPw().flush();
		String response = getBr().readLine();
		Scanner sc = new Scanner(response);
		String[] arg = {"", "", ""};
		for(int i=0; i<3 && sc.hasNext(); i++) {
			arg[i] = sc.next();
		}
		sc.close();
		switch(arg[0]) {
			case Command.OK:
				Socket getListUserSocket = new Socket(FTClient.getHost(), Integer.parseInt(arg[1]));
				BufferedReader br = new BufferedReader(new InputStreamReader(getListUserSocket.getInputStream()));
				String address;
				FTClient.getUserList().removeAll();
				while((address = br.readLine())!=null) {
					Scanner s = new Scanner(address);
					String addr = s.next();
					if(!FTClient.getUserList().getUserMap().containsKey(addr)) {
						FTClient.getUserList().addUser(new User(addr));
					}
					s.close();
				}
				br.close();
				getListUserSocket.close();
				Window.getInstance().updateTable();
				break;
			default:
				System.out.println("Rejected");
				break;
		}
	}
	
}
