package client.user;

public class User {

	private String address;
	private boolean send;
	private boolean block;
	
	public User() {
		setAddress("");
		setSend(false);
		setBlock(false);
	}
	
	public User(String address) {
		this();
		setAddress(address);
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public boolean isSend() {
		return send;
	}
	
	public void setSend(boolean send) {
		this.send = send;
	}
	
	public boolean isBlock() {
		return block;
	}
	
	public void setBlock(boolean block) {
		this.block = block;
	}
	
}
