package client.user;

import java.util.HashMap;

public class UserList {

	private HashMap<String, User> userMap;
	
	public UserList() {
		userMap = new HashMap<String, User>();
	}
	
	public HashMap<String, User> getUserMap() {
		return userMap;
	}

	public void setListUser(HashMap<String, User> userMap) {
		this.userMap = userMap;
	}
	
	public void addUser(User user) {
		if(!getUserMap().containsKey(user.getAddress())){
			getUserMap().put(user.getAddress(), user);
		}
	}
	
	public void removeUser(User user) {
		getUserMap().remove(user.getAddress());
	}
	
	public void removeAll() {
		getUserMap().clear();
	}
	
}