package server;

import java.io.IOException;

import server.common.Message;
import server.component.control.ControlServer;
import server.component.notif.NotifServer;

public class FTServer {

	private NotifServer notifServer;
	private ControlServer controlServer;
	
    public FTServer () throws IOException {
        setNotifServer(new NotifServer(this));
        setControlServer(new ControlServer(this));
    }

    public void startServer() throws IOException {
    	getNotifServer().start();
    	getControlServer().start();
    	System.out.println(Message.STARTING_SERVER);
    }

	public ControlServer getControlServer() {
		return controlServer;
	}

	public void setControlServer(ControlServer controlServer) {
		this.controlServer = controlServer;
	}

	public NotifServer getNotifServer() {
		return notifServer;
	}

	public void setNotifServer(NotifServer notifServer) {
		this.notifServer = notifServer;
	}
	
}
