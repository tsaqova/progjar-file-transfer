package server.abstraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public abstract class AThread implements Runnable {

	private Thread thread;
    private Socket socket;
    private AServer server;
    private BufferedReader br;
    private PrintWriter pw;
	
    public AThread() {
    	
    }
    
    public AThread(AServer server, Socket socket) throws IOException {
    	this();
		setServer(server);
		setSocket(socket);
    	setBr(new BufferedReader(new InputStreamReader(getSocket().getInputStream())));
    	setPw(new PrintWriter((getSocket().getOutputStream()))); 
    }
    
	public void start() {
    	if(thread == null) {
    		thread = new Thread(this);
    		thread.start();
    	}
	}
    
	public void removeFromMap() throws IOException {
		closeSocket();
		getServer().getThreadMap().remove(getHostAddress());
	}
	
	public void closeSocket() throws IOException {
		getBr().close();
		getPw().close();
		getSocket().close();
	}
	
	public String getHostAddress() {
		return this.socket.getInetAddress().getHostAddress();
	}
	
    public Thread getThread() {
		return thread;
	}
    
	public void setThread(Thread thread) {
		this.thread = thread;
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	public AServer getServer() {
		return server;
	}
	
	public void setServer(AServer server) {
		this.server = server;
	}
	
	public BufferedReader getBr() {
		return br;
	}
	
	public void setBr(BufferedReader br) {
		this.br = br;
	}
	
	public PrintWriter getPw() {
		return pw;
	}
	
	public void setPw(PrintWriter pw) {
		this.pw = pw;
	}
    
}
