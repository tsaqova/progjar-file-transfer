package server.abstraction;

import java.net.ServerSocket;
import java.util.HashMap;

import server.FTServer;

public abstract class AServer implements Runnable {
	
	private Thread thread;
	private ServerSocket serverSocket;
	private FTServer ftServer;
    private HashMap<String, AThread> threadMap;
    
    public AServer() {
    	setThreadMap(new HashMap<String, AThread>());
    }
	
	public void start() {
    	if(thread == null) {
    		thread = new Thread(this);
    		thread.start();
    	}
	}

	public FTServer getFtServer() {
		return ftServer;
	}

	public void setFtServer(FTServer ftServer) {
		this.ftServer = ftServer;
	}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}

	public HashMap<String, AThread> getThreadMap() {
		return threadMap;
	}

	public void setThreadMap(HashMap<String, AThread> threadMap) {
		this.threadMap = threadMap;
	}
	
}
