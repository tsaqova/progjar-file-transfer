package server.component.notif;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import server.FTServer;
import server.abstraction.AServer;
import server.common.Config;

public class NotifServer extends AServer {

    public NotifServer() throws IOException {
    	setServerSocket(new ServerSocket(Config.PORT_NOTIF));
    }
    
    public NotifServer(FTServer ftServer) throws IOException {
    	this();
    	setFtServer(ftServer);
    }
    
    @Override
	public void run() {
		while(true){
			try {
				Socket socket = getServerSocket().accept();
				NotifThread notifThread = new NotifThread(this, socket);
				getThreadMap().put(notifThread.getHostAddress(), notifThread);
				notifThread.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
