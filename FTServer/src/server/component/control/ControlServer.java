package server.component.control;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import server.FTServer;
import server.abstraction.AServer;
import server.common.Config;

public class ControlServer extends AServer {
    
    public ControlServer() throws IOException {
    	setServerSocket(new ServerSocket(Config.PORT_CTRL));
    }
    
    public ControlServer(FTServer ftServer) throws IOException {
    	this();
    	setFtServer(ftServer);
    }
    
    @Override
	public void run() {
		while(true){
			try {
				Socket socket = getServerSocket().accept();
				ControlThread controlThread = new ControlThread(this, socket);
				getThreadMap().put(controlThread.getHostAddress(), controlThread);
				controlThread.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
