package server.component.control;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Vector;

import server.abstraction.AThread;
import server.common.Command;
import server.common.Config;
import server.common.Message;
import server.component.notif.NotifServer;
import server.component.notif.NotifThread;

public class ControlThread extends AThread {
    
    public ControlThread(ControlServer commandServer, Socket socket) throws IOException {
    	super(commandServer, socket);
    }

    @Override
    public void run() {
    	try {
    		System.out.println(Message.ConnectedToServer(getHostAddress()));
			String msg;
			while((msg = getBr().readLine()) != null && !Thread.currentThread().isInterrupted()) {
				System.out.println(Message.ShowMessage(getHostAddress(), msg));
				try{
					GiveResponse(msg);
				}
				catch(NoSuchElementException ex) {
					ex.printStackTrace();
				}
			}
	    	removeFromMap();
	    	NotifThread notifThread = (NotifThread) getServer().getFtServer().getNotifServer().getThreadMap().get(getHostAddress());
	    	notifThread.removeFromMap();
	    	System.out.println(Message.DisconnectedFromServer(getSocket().getInetAddress().getHostAddress()));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    private void GiveResponse(String msg) throws IOException {
    	String[] arg = {"", "", ""};
    	SplitString(msg, arg);
    	switch(arg[0]) {
	    	case Command.STOR:
	    		ResponseStor(arg);
				break;
	    	case Command.LIST:
	    		ResponseList();
	    		break;
	    	default:
	    		break;
    	}
    }

    private void ResponseStor(String[] arg) throws IOException {
		getPw().println(Command.OK_DATA);
		getPw().flush();
		ReceiveDataFromSender(arg[2]);
		Vector<String> listUser = new Vector<String>();
		ProcessListUser(listUser, arg[1]);
		ServerSocket toReceiverDataSocket = new ServerSocket(Config.PORT_DATA);
		NotifServer notifServer = getServer().getFtServer().getNotifServer();
		for(String userAddress : listUser) {
			if(UserExist(userAddress, notifServer.getThreadMap())) {
				String response = SendNotification(notifServer, userAddress, arg[2]);
				if(response.equals(Command.OK)) {
					SendDataToClient(toReceiverDataSocket, arg[2]);
					System.out.println(Message.SendFileSuccess(userAddress));
				}
				else {
					System.out.println(Message.SendFileBlocked(userAddress));
				}
			}
			else if(!userAddress.equals(getHostAddress())){
				System.out.println("User " + userAddress + " doesnt exist");
			}
		}
		File file = new File(arg[2]);
		file.delete();
		toReceiverDataSocket.close();
	}
    
	private void ResponseList() throws IOException {
		getPw().println(Command.OK_DATA);
		getPw().flush();
		ServerSocket getListUserServerSocket = new ServerSocket(Config.PORT_DATA);
		Socket getListUserSocket = getListUserServerSocket.accept();
		PrintWriter pw = new PrintWriter(getListUserSocket.getOutputStream());
		NotifServer notifServer = getServer().getFtServer().getNotifServer();
		for(Entry<String, AThread> entry : notifServer.getThreadMap().entrySet()) {
			pw.println(entry.getValue().getHostAddress());
			getPw().flush();
		}
		pw.close();
		getListUserSocket.close();
		getListUserServerSocket.close();
		System.out.println(Message.SendListUser(getListUserSocket.getInetAddress().getHostAddress()));
	}

	private void SendDataToClient(ServerSocket toReceiverDataSocket, String fileName) throws IOException {
    	File myFile = new File(fileName);
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(myFile));
		Socket receiverSocket = toReceiverDataSocket.accept();
		byte[] mybytearray = new byte[(int) myFile.length()];
		bis.read(mybytearray, 0, mybytearray.length);
		OutputStream os = receiverSocket.getOutputStream();
		os.write(mybytearray, 0, mybytearray.length);
		os.flush();
		os.close();
		receiverSocket.close();
		bis.close();
	}

	private String SendNotification(NotifServer notifServer, String userAddress, String fileName) throws IOException {
		NotifThread notifThread = (NotifThread) notifServer.getThreadMap().get(userAddress);
		PrintWriter pwout = new PrintWriter(notifThread.getSocket().getOutputStream());
		BufferedReader bread = new BufferedReader(new InputStreamReader(notifThread.getSocket().getInputStream()));
		pwout.println(getHostAddress() + " " + Config.PORT_DATA + " " + fileName);
		pwout.flush();
		String response = bread.readLine();
		return response;
	}

	private boolean UserExist(String userAddress, HashMap<String, AThread> threadMap) {
    	if (threadMap.containsKey(userAddress) && !getHostAddress().equals(userAddress)) {
    		return true;
    	}
    	else {
    		return false;
    	}
	}

	private void ProcessListUser(Vector<String> listUser, String arg) {
		Scanner sc = new Scanner(arg);
		sc.useDelimiter(";");
		while(sc.hasNext()) {
			listUser.add(sc.next());
		}
		sc.close();
	}

	private void ReceiveDataFromSender(String fileName) throws IOException {
		ServerSocket dataServerSocket = new ServerSocket(Config.PORT_DATA);
		Socket dataSocket = dataServerSocket.accept();
		WriteToFile(dataSocket, fileName);
		dataSocket.close();
		dataServerSocket.close();
	}

	private void SplitString(String msg, String[] arg) {
		Scanner sc = new Scanner(msg);
		int i;
		for(i=0; i<3 && sc.hasNext(); i++) {
			arg[i] = sc.next();
		}
		if(sc.hasNextLine())arg[2] += sc.nextLine();
		sc.close();
    }
    
    private void WriteToFile(Socket dataSocket, String fileName) throws IOException {
		byte[] bytearray = new byte[Config.BUFFER_SIZE_INT];
		InputStream is = dataSocket.getInputStream();
		FileOutputStream fos = new FileOutputStream(fileName);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		int bytesRead;
		while((bytesRead = is.read(bytearray)) > 0) {
			bos.write(bytearray, 0, bytesRead);
		}
		bos.flush();
		is.close();
		fos.close();
		bos.close();
    }
	
}
