package server.common;

public final class Message {

	public static final String STARTING_SERVER = "Starting File Transfer Server...";
	public static final String SERVER = "SERVER";
	
	public static final String ShowMessage(String address, String msg) {
		return address + " : " + msg;
	}
	
	public static final String SendFileSuccess(String userAddress) {
		String msg = "Transfer file to " + userAddress + " success";
		return ShowMessage(SERVER, msg);
	}
	
	public static final String SendFileBlocked(String userAddress) {
		String msg = "Transfer file to " + userAddress + " blocked";
		return ShowMessage(SERVER, msg);
	}
	
	public static final String SendListUser(String userAddress) {
		String msg = "Send List of User to " + userAddress;
		return ShowMessage(SERVER, msg);
	}
	
	public static final String ConnectedToServer(String address) {
		return ShowMessage(address, "connected to server");
	}
	
	public static final String DisconnectedFromServer(String address) {
		return ShowMessage(address, "disconnected from server");
	}
	
}
