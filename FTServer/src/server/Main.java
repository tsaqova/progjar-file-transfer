package server;

import java.io.IOException;

public class Main {
	
	public static void main(String[] args) {
		try {
			FTServer ftServer = new FTServer();
			ftServer.startServer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}